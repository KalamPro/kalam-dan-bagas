<!-- 


KALAM MAHARDHIKA
RADEN BAGAS WASTU WIRATAMA


 -->

<?php  

function terbilang( $rupiah ) {

	// Variabel angka yang berisi array kosong untuk menginisialisasikan bahwa variabel ini adalah array 
	$angka = [];

	// Memberikan isi array tersebut dalam bentuk string yang berisi jumlah terbilang sampai dengan sebelas
	$angka = ["","Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas"];

	//Kondisi apabila rupiah kurang dari 12, biar langsung mengambil data dari vavriabel angka
	if( $rupiah < 12 ) {
		/*
		Mengembalikan nilai angka yang diinputkan oleh user yang didapatkan dari variabel angka dengan berisikan index inputan, karena inputan dibawah 12 aada datanya di variabel angka
		*/
		return " " . $angka[$rupiah];
		
	}

	//kondisi apabila angka yang diinputkan kurang dari 20
	else if( $rupiah < 20 ) {
		/*
		Mengembalikan function yang berisi angka yang diinputkan kemudian dikurangi 10 lalu menggabungkannya dengan String belas, karena angka dibawan 20 pasti berbentuk nilai belasan 
		return terbilang( $rupiah - 10 ) . " Belas";
		*/
	}

	// Kondisi untuk menghitung puluhan
	else if( $rupiah < 100 ) {
		/*
		Mengembalikan function yang berisi angka yang diinputkan kemudian dibagi 10, karena angka yang diinputkan berjumlah puluhan, lalu menggabungkannya dengan String puluh, karena angka dibawan 100 pasti berbentuk nilai puluhan, lalu digabungkan dengan function dengan berisikan inputan yang dimodulus dengan 10 (karena puluhan tea) agar angka sisa baginya dapat di olah kembali oleh function "terbilang"
		*/
		return terbilang( $rupiah / 10 ) . " Puluh" . terbilang( $rupiah % 10 );
	}

	// Kondisi untuk menghitung angka ratusan
	else if( $rupiah < 1000 ) {
		/*
		Mengembalikan function yang berisi angka yang diinputkan kemudian dibagi 100, karena angka yang diinputkan berjumlah ratusan, lalu menggabungkannya dengan String ratus, karena angka dibawan 1000 pasti berbentuk nilai ratusan, lalu digabungkan dengan function dengan berisikan inputan yang dimodulus dengan 100 (karena ratusan tea) agar angka sisa baginya dapat di olah kembali oleh function "terbilang"
		*/
		return terbilang( $rupiah / 100 ) . " Ratus " . terbilang( $rupiah % 100 );
	}

	// Kondisi untuk menghitung angka ribuan
	else if( $rupiah < 1000000 ) {
		/*
		Mengembalikan function yang berisi angka yang diinputkan kemudian dibagi 1.000, karena angka yang diinputkan berjumlah ribuan, lalu menggabungkannya dengan String ribu, karena angka dibawan 1000000 pasti berbentuk nilai ribuan, lalu digabungkan dengan function dengan berisikan inputan yang dimodulus dengan 1.000 (karena ribuan tea) agar angka sisa baginya dapat di olah kembali oleh function "terbilang"
		*/
		return terbilang( $rupiah / 1000 ) . " Ribu " . terbilang( $rupiah % 1000 );
	}
	// Kondisi untuk menghitung angka jutaan
	else if( $rupiah < 1000000000 ) {
		/*
		Mengembalikan function yang berisi angka yang diinputkan kemudian dibagi 1.000.000, karena angka yang diinputkan berjumlah jutaan, lalu menggabungkannya dengan String juta, karena angka dibawan 1.000.000.000 pasti berbentuk nilai jutaan, lalu digabungkan dengan function dengan berisikan inputan yang dimodulus dengan 1.000.000 (karena jutaan tea) agar angka sisa baginya dapat di olah kembali oleh function "terbilang"
		*/
		return terbilang( $rupiah / 1000000 ) . " Juta". terbilang( $rupiah % 1000000 );
		
	}
	// Kondisi untuk menghitung angka milyar
	else if( $rupiah < 1000000000000 ) {
		/*
		Mengembalikan function yang berisi angka yang diinputkan kemudian dibagi 1.000.000.000, karena angka yang diinputkan berjumlah milyaran, lalu menggabungkannya dengan String milyar, karena angka dibawan 1.000.000.000.000 pasti berbentuk nilai milyaran, lalu digabungkan dengan function dengan berisikan function fmod yang berisi nilai inputan dan angka 1.000.000.000 (karena milyaran tea) yang akan menghasilkan nilai hasil bagi, karena function fmod berfungsi untuk mengembalikan nilai sisa bagi.
		*/
		return terbilang( $rupiah / 1000000000 ) . " Milyar" . terbilang( fmod( $rupiah, 1000000000 ) );
		
	}
	// Kondisi untuk menghitung angka triliyun
	else if( $rupiah < 1000000000000000 ) {
		/* 
		Mengembalikan function yang berisi angka yang diinputkan kemudian dibagi 1.000.000.000.000, karena angka yang diinputkan berjumlah triliyunan, lalu menggabungkannya dengan String milyar, karena angka dibawan 1.000.000.000.000.000 pasti berbentuk nilai triliyunan, lalu digabungkan dengan function dengan berisikan function fmod yang berisi nilai inputan dan angka 1.000.000.000.000 (karena triliyunan tea) yang akan menghasilkan nilai hasil bagi, karena function fmod berfungsi untuk mengembalikan nilai sisa bagi.
		*/
		return terbilang( $rupiah / 1000000000000 ) . " Trilyun" . terbilang( fmod( $rupiah, 1000000000000 ) );
		
	}
	else{
		//kondisi apabila inputan berlebih dan menampilkan pemberitahuan dengan bentuk string
		echo "Maaf Terlalu Banyak yang Anda Inputkan";
	}

}


?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Terbilang</title>
</head>
<body>
	<fieldset>
	<!-- 
	Form disini berguna menginputkan sebuah nilai yang nantinya akan dikirimkan dengan menggunakan method POST
	 -->
	<form action="" method="post">
		
		<!--  tag input disini berguna untuk memasukan sebuah nilai nominal -->
		<input type="number" name="rupiah" placeholder="Masukan Nominal" autocomplete="off">
		<!-- button ini berfungsi untuk megnirimkan nilai yang sudah diinputkan dan kemudian dikirimkan dengan method POST -->
		<button type="submit" name="hasil">Hasil</button>

	</form>
	<hr style="width: 250px;margin-left: 0px;">

	<?php  

	// Kondisi disini berfungsi apabila terdapat method POST 
	if( $_POST ) {

		// number_format(angka,angka_diblkng_koma, pemisah_desimal, pemisah ribuan)
		// ucword()...memanipulasi string, jadi setiap awalan kata akan jadi huruf kapital
		echo "Rp." . number_format($_POST["rupiah"], 0, ",", ".");
		echo "<br>";
		echo " (";
		// Memanggil function terbilang yang berisi parameter rupiah yang didapatkan dari metode post.
		echo ucwords(terbilang($_POST["rupiah"] )) . " Rupiah";
		echo ")";

	}

	?>
	</fieldset>
	
</body>
</html>